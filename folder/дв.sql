CREATE DATABASE Library;
use Library;

Create table dbo.Author(
[AuthorNo] int not null IDENTITY(1,1),
[AuthorSurname] nvarchar(128) not null,
[AuthorName] nvarchar(128) not null,
[AuthorMiddlename] nvarchar(128) null,
[AuthorBirthday] date null,
[AuthorCountry] nvarchar(30) not null,
constraint [Author$PK] primary key (AuthorNo) 
);
